"""
    Trees-preorder is to learn the basic traversal.

    Given a pre-order list, print tree in pre-order
"""
class Node():
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

# Build tree recursively
def buildTree(values):
    length = len(values)
    root = None

    return _buildTreeHelper(values, root, 0, length)

def _buildTreeHelper(values, node, i, length):
    if i < length:
        temp = values[i]
        # print("Hit value: ", temp)

        if temp != None:
            node = Node(temp)
        else:
            return node

        node.left = _buildTreeHelper(values, node.left, 2*i+1, length)  # Left child: 2*i+1
        node.right = _buildTreeHelper(values, node.right, 2*i+2, length) # Right child: 2*i+2

        # if node.left != None:
        #     print("Node ", node.data, " left child: ", node.left.data)
        # if node.right != None:
        #     print("Node ", node.data, " right child: ", node.right.data)

    return node





# Prints tree recursively
def printTree(root):
    _printTreeHelper(root)

def _printTreeHelper(node):
    if node != None:
        print(node.data, end=" ")
        _printTreeHelper(node.left)
        _printTreeHelper(node.right)


# Print tree iteratively  BAD
# def printTreeIteratively(root):
#     results = []
#     stack = []
#     stack.append(root)

#     while len(stack) > 0:
#         node = stack.pop()

#         # Deadend node
#         if node.data != None:
#             results.append(node.data)
#             # print ("Hit node: ", node.data)

#             if node.right != None:
#                 stack.append(node.right)
#             if node.left != None:
#                 stack.append(node.left)

#     print(results)

if __name__ == "__main__":
    # Basic traversal
    root = buildTree([1,None,2,None,None,3])
    printTree(root)
    # printTreeIteratively(root)

    # # Advance test case
    # root = buildTree([2,0,None,1,3,None,4])
    # printTree(root)
    # printTreeIteratively(root)