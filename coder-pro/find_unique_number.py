from collections import defaultdict

class Solution(object):
    """
        Time    O(2n)
        Space   O(n)
    """
    def singleNumber(self, nums:list):
        seen = defaultdict(int)
        
        for n in nums:          # O(n)
            seen[n] += 1

        
        for s in seen:          # O(m)
            if seen[s] == 1:
                return s

    """
        Time    O(n)
        Space   O(1)
    """
    def singleNumber2(self, nums):
        unique = 0
        for n in nums:
            unique ^= n
        return unique

print(Solution().singleNumber([2,2,1]))
print(Solution().singleNumber([4,1,2,1,2]))


print(Solution().singleNumber2([4,1,2,1,2]))