class Solution(object):
    def numIslands(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """

        def visit(r, c):
            if r < 0 or r == len(grid) or c < 0 or c == len(grid[r]):
                return

            elif grid[r][c] == '1':
                grid[r][c] = '0'
                visit(r+1, c)
                visit(r-1, c)
                visit(r, c+1)
                visit(r, c-1)

        islands = 0
        for r in range(len(grid)):
            for c in range(len(grid[r])):
                if grid[r][c] == '1':
                    # print('Visit', r, c)
                    visit(r, c)
                    # print(grid)
                    islands += 1

        return islands


# print(Solution().numIslands([
#     ['1', '1', '1', '1', '0'],
#     ['1', '1', '0', '1', '0'],
#     ['1', '1', '0', '0', '0'],
#     ['0', '0', '0', '0', '0'],
# ]))


print(Solution().numIslands([
    ["1", "1", "0", "0", "0"], 
    ["1", "1", "0", "0", "0"],
    ["0", "0", "1", "0", "0"],
    ["0", "0", "0", "1", "1"]]))
