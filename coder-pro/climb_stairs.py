class Solution(object):
    def climbStairs(self, n):
        paths = {1:1, 2:2}

        if n in paths:
            return paths[n]
        
        for i in range(3, n+1):
            paths[i] = paths[i-1] + paths[i-2]

        return paths[n]
        
print(Solution().climbStairs(2)) #2
print(Solution().climbStairs(5)) #8
