""" 
    For reference
    Did not make runnable
"""

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: int
        :type q: int
        :rtype: TreeNode
        """
        
        def traceNode(node, target, trace):
            trace.append(node)
            
            if node.val == target.val:
                return trace
            elif node.val > target.val:
                return traceNode(node.left, target, trace)
            else:
                return traceNode(node.right, target, trace)
            
        
        node = root
        queue1 = traceNode(node, p, [])
        node = root
        queue2 = traceNode(node, q, [])
        
        lca = None
        while stack1 or stack2:
            if stack1:
                n1 = queue1.pop(0)
            if stack2:
                n2 = queue2.pop(0)
            
            if n1 and n2:
                if n1.val == n2.val:
                    lca = n1
        return lca