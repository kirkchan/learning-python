from collections import defaultdict

class Solution:
    def lengthOfLongestSubstring(self, s):
        letters = defaultdict(int)
        longest = 0
        i,j = 0,0

        while j < len(s):
            # Count up of current letter
            letters[s[j]] += 1

            if letters[s[j]] <= 1: # Unique
                longest = max(longest, j-i+1)
                print('Longest?: [{}]'.format(s[i:j+1]))
            else: # Duplicate
                while letters[s[j]] > 1:
                    letters[s[i]] -= 1
                    i += 1

            j += 1

        return longest

print ('Answer:', Solution().lengthOfLongestSubstring('abrkaabcdefghijjxxx')) # 10
print ('Answer:', Solution().lengthOfLongestSubstring('bbbb')) # 1
print ('Answer:', Solution().lengthOfLongestSubstring('abcdefgbbbb')) # 7
print ('Answer:', Solution().lengthOfLongestSubstring('aaaaaabcdefgbbbb')) # 7
print ('Answer:', Solution().lengthOfLongestSubstring(" ")) # 1
print ('Answer:', Solution().lengthOfLongestSubstring('au')) # 2
print ('Answer:', Solution().lengthOfLongestSubstring('dvdf')) # 2