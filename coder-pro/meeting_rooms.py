class Solution(object):
    def minMeetingRooms(self, interverals) -> int:
        start = []
        end = []

        for i in interverals:
            start.append(i[0])
            end.append(i[1])

        start.sort()
        end.sort()

        available = 0
        capacity = 0

        s = 0
        e = 0
        
        while s < len(start):
            if start[s] < end[e]:
                s += 1
                if available == 0:
                    capacity += 1
                else:
                    available -= 1
            else:
                available += 1
                e += 1

        return capacity


print(Solution().minMeetingRooms([[0,30], [5,10], [15,20]]))