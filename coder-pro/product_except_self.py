class Solution(object):
  def productExceptSelf(self, nums):
    N = len(nums)
    res = [1] * N
    for i in range(1, N):
      res[i] = res[i-1] * nums[i-1]
    right = 1
    for i in range(N - 2, -1, -1):
      right *= nums[i+1]
      res[i] *= right
    return res



if __name__ == "__main__":
  print(Solution().productExceptSelf([1,2,3,4])) # [24,12,8,6]