# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x, next):
        self.val = x
        self.next = next

class Solution(object):
    def removeZeroSumSublists(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        cursor = head
        if not cursor or cursor.next == None:
            return head

        hadZeroSum = False
        while cursor:
            if cursor.next:
                val = cursor.val + cursor.next.val
                print('Val', val)

                if val == 0:
                    if cursor.next.next:
                        cursor.val = cursor.next.next.val
                        cursor.next = cursor.next.next.next
                    else:
                        cursor = None

                if cursor:
                    print(cursor.val)
                    cursor = cursor.next

        return head

head = Solution().removeZeroSumSublists(ListNode(1, ListNode(2, ListNode(-2, None)))) # 1

while head:
    print(head.val, end=' ')
    head = head.next