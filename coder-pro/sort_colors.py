class Solution(object):
    def sortColors(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        p0 = 0
        p1 = 0
        p2 = len(nums)-1

        while p1 <= p2:
            if nums[p1] == 0:
                nums[p1], nums[p0] = nums[p0], nums[p1]
                p1+=1
                p0+=1
            elif nums[p1] == 1:
                p1+=1
            else:
                nums[p1], nums[p2] = nums[p2], nums[p1]
                p2-=1
                
        return nums

print(Solution().sortColors([2,0,2,1,1,0]));
print(Solution().sortColors([2,0,1]));
print(Solution().sortColors([1,2,0]));