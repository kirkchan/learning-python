def findPythagoreanTriplets(nums):
    squares = []

    for n in nums:
        squares.append(n**2)

    squares.sort(reverse=True)
    print(squares)

    for i in range(len(squares)-1):
        for j in range(i+1, len(squares)):
            third = squares[i]-squares[j]

            if third in squares:
                print(squares[i], squares[j], third)
                return True
    return False


print(findPythagoreanTriplets([3, 5, 12, 5, 13]))
# True