class MyQueue(object):

    def __init__(self):
        self.stack = []        

    def push(self, x):
        self.stack.append(x)
        
    def pop(self):
        if self.empty():
            return None

        temp = []
        while self.stack:
            temp.append(self.stack.pop())
            
        resp = temp.pop()
        while temp:
            self.stack.append(temp.pop())

        return resp

    def peek(self):
        if self.empty():
            return None

        temp = []
        while self.stack:
            temp.append(self.stack.pop())
        
        resp = temp[len(temp)-1]
        while temp:
            self.stack.append(temp.pop())

        return resp

    def empty(self):
        return len(self.stack) == 0
        


# Your MyQueue object will be instantiated and called as such:
obj = MyQueue()
print(obj.push(1))
print(obj.push(2))
print(obj.peek())
print(obj.pop())
print(obj.empty())