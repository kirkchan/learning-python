""" 
    Time    O(n**2)
    Space   O(n)

    Trick here is to realize taller, once placed, are unaffected by shorter
    people insertions. Then add based on their position.
"""
class Solution(object):
    def reconstructQueue(self, people:list):
        people.sort(key=lambda x: (-x[0], x[1]))  # O(nlogn)
        # print(people)
        res = []
        for p in people:  # O(N)
            # print(res)
            res.insert(p[1], p)  # O(n)
        return res


print(Solution().reconstructQueue([[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]))
# [[5,0], [7,0], [5,2], [6,1], [4,4], [7,1]]