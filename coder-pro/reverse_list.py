class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None
  
    # Function to print the list
    def printList(self):
        node = self
        output = '' 
        while node != None:
            output += str(node.val)
            output += " "
            node = node.next
        return output

  # Iterative Solution
    def reverseIteratively(self, head):
        last = None
        crnt = head

        while crnt:
            node = ListNode(crnt.val)
            node.next = last
            last = node
            crnt = crnt.next
        
        return last

    # Recursive Solution
    def reverseRecursively(self, head):

        def helper(crnt, last):
            if crnt == None:
                return last

            node = ListNode(crnt.val)
            node.next = last
            node = helper(crnt.next, node)

            return node
        
        return helper(head, None)

# Test Program
# Initialize the test list: 
testHead = ListNode(4)
node1 = ListNode(3)
testHead.next = node1
node2 = ListNode(2)
node1.next = node2
node3 = ListNode(1)
node2.next = node3
testTail = ListNode(0)
node3.next = testTail

print("Initial list: ", testHead.printList())
# 4 3 2 1 0
testTail = testHead.reverseIteratively(testHead)
print("List after reversal iteratively: ", testTail.printList())
testTail = testHead.reverseRecursively(testHead)
print("List after reversal recursively: ", testTail.printList())
# 0 1 2 3 4