class Node(object):
    def __init__(self, val, children=[]):
        self.val = val
        self.children = children

    def isClone(self, node):
        if self.val != node.val:
            return False

        if len(self.children) != len(node.children):
            return False
        
        for i in range(len(self.children)):
            return self.children[i].isClone(node.children[i])
        
        return True

n1 = Node(1)
n1.children = [Node(2), Node(3)]
n1.children[0].children = [Node(4), Node(5)]

n2 = Node(1)
n2.children = [Node(2), Node(3)]
n2.children[0].children = [Node(4), Node(5)]

print(n1.isClone(n2))