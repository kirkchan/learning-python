import collections

"""
    Great problem; tricky solution
    GOAL: Seek loops
    State must only be kept during DFS
"""
class Solution(object):
    def canFinish(self, numCourses, prerequisites):
        graph = collections.defaultdict(list)
        for edge in prerequisites:
            graph[edge[0]].append(edge[1])

        visited = set()

        # True if there is a cycle, False if not
        def visit(vertex):
            visited.add(vertex)
            for neighbour in graph[vertex]:
                if neighbour in visited or visit(neighbour):
                    return True
            visited.remove(vertex)
            return False

        for i in range(numCourses):
            if visit(i):
                return False
        return True

print(Solution().canFinish(2, [[0,1]])) # True
print(Solution().canFinish(2, [[1,0]])) # True
print(Solution().canFinish(2, [[1,0],[0,1]])) # False
print(Solution().canFinish(3, [[0,1],[1,2],[2,0]])) # False
print(Solution().canFinish(6, [[0,1],[3,2],[4,3]])) # True
print(Solution().canFinish(3, [[1,0],[2,1]])) # True
print(Solution().canFinish(3, [[0,1],[0,2],[1,2]])) # True