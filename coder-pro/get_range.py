import math

class Solution: 
    """ 
        O(n) solution
        Challenge solve in O(log(n))
    """
    # def getRange(self, arr, target):
    #     try:
    #         first = arr.index(target)
            
    #         for i in (range(first, len(arr))):
    #             if arr[i] != target:
    #                 return [first, i-1]
    #     except ValueError:
    #         return [-1, -1]
        

    #     return [-1, -1]

    """
        Optimized solution via binary search of array
    """
    def getRange(self, arr, target):

        def binarySearch(left, right, i, isLow):
            if left <= right:    
                mid = math.floor((left + right) / 2)

                if isLow:
                    if arr[mid] == target:
                        i = min(mid, i)
                else:
                    if arr[mid] == target:
                        i = max(mid, i)

                i = binarySearch(left, mid-1, i, isLow) # New left
                i = binarySearch(mid+1, right, i, isLow) # New right

            return i
        
        low = binarySearch(0, len(arr)-1, len(arr), True)
        high = binarySearch(0, len(arr)-1, -1, False)

        if low == len(arr) or high == -1:
            return [-1,-1]

        return [low,high]
  


print(Solution().getRange([1, 2, 2, 2, 2, 3, 4, 7, 8, 8], 2))
# [1, 4]

print(Solution().getRange([1,3,3,5,7,8,9,9,9,15], 9))
# # [6,8]

print(Solution().getRange([100, 150, 150, 153], 150))
# # [1,2]

print(Solution().getRange([1,2,3,4,5,6,10], 9))
# [-1,-1]
