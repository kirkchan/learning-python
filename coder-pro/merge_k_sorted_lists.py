from collections import defaultdict

# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x, next=None):
        self.val = x
        self.next = next

class Solution(object):
    def mergeKLists(self, lists):
        valCount = defaultdict(int)
        seen = set()

        for l in lists:
            while l:
                valCount[l.val] += 1
                seen.add(l.val)
                l = l.next
        
        keys = sorted(seen)
        head = ListNode(0)
        crnt = head

        for k in keys:
            times = valCount[k]

            for i in range(times):
                crnt.next = ListNode(k)
                crnt = crnt.next

        return head.next

i1 = ListNode(1, ListNode(4, ListNode(5)))
i2 = ListNode(1, ListNode(3, ListNode(4)))
i3 = ListNode(2, ListNode(6))

kNodes = [i1, i2, i3]
head = Solution().mergeKLists(kNodes)

while head:
    print(head.val)
    head = head.next