class Solution(object):
    def spiralOrder(self, matrix):
        directionals = [[0,1], [1,0], [0,-1], [-1,0]]
        d = 0
        x,y = 0,0
        res = []

        remaining = len(matrix)*len(matrix[0])

        def _traverse(dir, x, y, remaining):
            while True:
                if matrix[x][y] == None:
                    x+=dir[0]
                    y+=dir[1]
                    continue
                res.append(matrix[x][y])
                matrix[x][y] = None
                remaining = remaining - 1

                if x+dir[0] >= 0 and x+dir[0] < len(matrix):
                    x += dir[0]
                else:
                    break

                if y+dir[1] >= 0 and y+dir[1] < len(matrix[0]):
                    y += dir[1]
                else:
                    break

                if matrix[x][y] == None:
                    x -= dir[0]
                    y -= dir[1]
                    break

            return [x,y,remaining]

        while remaining > 0:
            dir = directionals[d%4]
            x,y,remaining = _traverse(dir, x, y, remaining)
            d+=1
        return res

        


print(Solution().spiralOrder([
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9,10,11,12]
]))     # [1,2,3,4,8,12,11,10,9,5,6,7]
