class Solution(object):
    def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 1
        
        seen = {}
        for n in nums:
            seen[n] = True
        
        
        for i in range(1, len(nums)+1):
            print(i in seen);
            if i not in seen:
                return i

        return len(nums)+1
        

print(Solution().firstMissingPositive([1,2,0])); # 3
print(Solution().firstMissingPositive([3,4,-1,1])); # 2
print(Solution().firstMissingPositive([7,8,9,11,12])); # 1
print(Solution().firstMissingPositive([1])); # 2
