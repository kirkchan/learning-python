# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def dfs(node, depth):
            if not node:
                return depth

            left = dfs(node.left, depth+1)
            right = dfs(node.right, depth+1)

            return max(left,right)

        return dfs(root, 0)