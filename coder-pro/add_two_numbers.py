class Node:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def addTwoNumbers(self, l1, l2):
        ans = Node(0)
        crnt = ans
        carryover = 0

        while l1 or l2 or carryover:
            sum = 0

            if l1:
                sum += l1.val
                l1 = l1.next
            if l2:
                sum += l2.val
                l2 = l2.next
            if carryover:
                sum += carryover

            crnt.next = Node(sum%10)
            crnt = crnt.next
            carryover = sum // 10

        return ans.next

def makeList(array):
    head = Node(0)
    crnt = head

    for n in array:
        crnt.next = Node(n)
        crnt = crnt.next
    
    return head.next

def printList(node):
    while node:
        print(node.val, end = ' ')
        node = node.next
    print()

l1 = makeList([2,4,3])
l2 = makeList([5,6,4])
printList(Solution().addTwoNumbers(l1, l2))

l1 = makeList([0,0,3])
l2 = makeList([0,0,4])
printList(Solution().addTwoNumbers(l1, l2))

l1 = makeList([6,6,3])
l2 = makeList([4,4,7])
printList(Solution().addTwoNumbers(l1, l2))