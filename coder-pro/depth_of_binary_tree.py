# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxDepth(self, root):
        
        def dfs(node, depth):
            left = right = 0
            if node.left:
                left = dfs(node.left, depth+1)
            if node.right:
                right = dfs(node.right, depth+1)
                
            return max([left,right,depth])

        if root:
            return dfs(root, 1)
        else:
            return 0