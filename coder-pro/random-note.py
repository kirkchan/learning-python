from collections import defaultdict

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str):
        """ 
            Build dict of magazine letters
            dict[a] = 2

            Loop thru ransomNote letters
            Subtract a letter from dict
            if neg
                return False
        """

        # Defaultdict is optimized to reduce conditions for non-existent keys
        available = defaultdict(int)
        for c in magazine:
                available[c] += 1
        for c in ransomNote:
            available[c] -= 1
            if available[c] < 0:
                return False        
        return True

print(Solution().canConstruct('aa', 'aab'))
print(Solution().canConstruct('aa', 'aba'))
print(Solution().canConstruct('aac', 'aab'))
# True