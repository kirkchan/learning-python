# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """

        def dfs(node, depth):
            if not node:
                return depth

            left = dfs(node.left, depth+1)
            right = dfs(node.right, depth+1)

            if abs(left-right) > 1:
                return -1

            return max(left,right)

        d = dfs(root, 0)

        if d == -1:
            return False
        return True