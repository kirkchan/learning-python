from collections import defaultdict

""" 
    Time Complexity     O(n)
    Space Complexity    O(n)
 """
def sortNums(nums):
    count = defaultdict(int)

    for n in nums:
        count[n] += 1

    i = 0
    for key in reversed(count):
        for n in range(count[key]):
            nums[i] = key
            i += 1

    return nums

""" 
    Time    O(n)
    Space   O(1)

    Sort in place
"""
def sortNums2(nums):
    i = 0           # Ones
    j = 0           # Current
    k = len(nums)-1 # Threes

    while j <= k:
        if nums[j] == 1:
            nums[j],nums[i] = nums[i],nums[j]
            i+=1
            j+=1
        if nums[j] == 2:
            j+=1
        if nums[j] == 3:
            nums[j],nums[k] = nums[k],nums[j]
            k-=1
    return nums

print(sortNums([3, 3, 2, 1, 3, 2, 1]))
print(sortNums2([3, 3, 2, 1, 3, 2, 1]))
# [1, 1, 2, 2, 3, 3, 3]