class MaxStack():
    def __init__(self):
        self.stack = []
        self.maxes = []

    def push(self, entry):
        self.stack.append(entry)
        if self.maxes:
            self.maxes.append(max(self.maxes[-1], entry))
        else:
            self.maxes.append(entry)

    def pop(self):
        if self.maxes:
            self.maxes.pop()
        return self.stack.pop()

    def max(self):
        return self.maxes[-1]


s = MaxStack()
s.push(1)
s.push(2)
s.push(3)
s.push(2)
print('max', s.max())
print(s.pop())
print('max', s.max())
print(s.pop())
print('max', s.max())
print(s.pop())
print('max', s.max())
print(s.pop())