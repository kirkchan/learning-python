""" 
    Ignored stack space
    Solves all but one weird extreme case on leetcode #79

    Time    NxM
    Space   NxM
"""

class Solution(object):
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """

        def constructVisited(N,M):
            v = []
            for x in range(N):
                v.append([False]*M)

            return v

        if not word or len(word) == 0:
            return True

        N = len(board)       # rows
        M = len(board[0])    # columns

        def findWord(i, j, visited, search):
            # Empty search -> word exists!
            if not search:
                return True

            # Out-of-bounds
            if i >= N or i < 0 or j >= M or j < 0:
                return False

            if visited[i][j]:
                return False
            
            visited[i][j] = True

            # print(N, M)
            # print(i, j)
            # print()
            # Matching char; proceed
            if board[i][j] == search[0]:

                if findWord(i+1, j, visited, search[1:]):
                    return True
                if findWord(i, j+1, visited, search[1:]):
                    return True
                if findWord(i-1, j, visited, search[1:]):
                    return True
                if findWord(i, j-1, visited, search[1:]):
                    return True

            # Backtracking
            visited[i][j] = False
            return False

        for a in range(N):
            for b in range(M):
                visited = constructVisited(N, M)

                # print('Searching from', a, b)
                if findWord(a, b, visited, word):
                    return True
        
        return False

if __name__ == "__main__":
    board = [
        ['A','B','C','E'],
        ['S','F','C','S'],
        ['A','D','E','E']
    ]

    print(Solution().exist(board, 'ABCCED')) # True
    print(Solution().exist(board, 'SEE')) # True
    print(Solution().exist(board, 'ABCB')) # False


    board = [
        ['A', 'B'],
        ['C', 'D']
    ]

    print(Solution().exist(board, 'BACD')) # True