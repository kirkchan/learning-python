class Node(object):
  def __init__(self, val):
    self.val = val
    self.left = None
    self.right = None

""" 
Positive unival subtrees
    Case 1: Leaf node
    Case 2: Unival from origin node
"""
def count_unival_subtrees(root):

    def dfs(node, unival): # returns (count, unival)
        if not node:
            return (0, None)

        (countL, univalL) = dfs(node.left, unival)
        (countR, univalR) = dfs(node.right, unival)

        # Always count leafs
        if univalL == None and univalR == None:
            return (1, node.val)
        
        c = 0
        if univalL == node.val and univalR == node.val:
            c+=1
        return (countL+countR+c, node.val)
            

    return dfs(root, root.val)[0]




#    0
#   / \
#  1   0
#     / \
#    1   0
#   / \
#  1   1
a = Node(0)
a.left = Node(1)
a.right = Node(1)
a.right.left = Node(1)
a.right.right = Node(1)
a.right.left.left = Node(1)
a.right.left.right = Node(1)

print(count_unival_subtrees(a))
# 5