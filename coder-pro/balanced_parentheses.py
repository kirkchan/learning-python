class Solution:
	def isValid(self, s):
		if len(s) == 0:
			return True

		pairs = {}
		pairs['}'] = '{'
		pairs[']'] = '['
		pairs[')'] = '('

		stack = []
		
		for i in range(len(s)):
			# Opening
			if not s[i] in pairs.keys():
				stack.append(s[i])
			# Closing
			elif len(stack) == 0 or pairs[s[i]] != stack.pop():
				return False

		# Leftovers means imbalanced after traversal
		return len(stack) == 0


# Test Program
s = "()(){(())" 
# should return False
print(Solution().isValid(s))

s = ""
# should return True
print(Solution().isValid(s))

s = "([{}])()"
# should return True
print(Solution().isValid(s))