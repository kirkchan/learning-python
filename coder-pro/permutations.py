class Solution:
    def permute(self, nums):
        res = []

        def helper(index):
            if index == len(nums)-1:
                print(nums[:])
                res.append(nums[:]) # Test later

            for i in range(index, len(nums)):
                nums[index],nums[i] = nums[i],nums[index]
                # print('i[{}]: {}'.format(i, nums))
                helper(index+1)
                nums[index],nums[i] = nums[i],nums[index]
            
        helper(0)
        return res
        

ans = Solution().permute([1,2,3])

for a in ans:
    print(a, end=' ')

""" Input: [1,2,3]
Output:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
] """