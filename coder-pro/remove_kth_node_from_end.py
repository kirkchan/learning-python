# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x, next):
        self.val = x
        self.next = next

    def __str__(self):
        n = self
        res = ''
        while n:
            res += str(n.val)
            n = n.next
        return res

class Solution(object):
    def removeNthFromEnd(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        cursor = head
        i = 0
        while cursor != None and i < n:
            cursor = cursor.next
            i+=1

        crnt = head
        while cursor.next != None:
            cursor = cursor.next
            crnt = crnt.next
    
        crnt.next = crnt.next.next

        return head

# 1->2->3->4->5, and n = 2.
ll = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5, None)))))
print(Solution().removeNthFromEnd(ll, 2)) # 1->2->3->5
ll = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5, None)))))
print(Solution().removeNthFromEnd(ll, 1))