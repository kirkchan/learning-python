""" 
    Can the array swap one or less elements to form an non-decreasing array?

    non-decreasing array: n[i] <= n[i + 1]
"""
class Solution(object):
    def checkPossibility(self, nums):
        idip= None

        for i in range(len(nums)-1):
            if nums[i] > nums[i+1]:
                if idip is not None:  # Two dips
                    return False
                idip=i
        
        if (idip is None or # No dip
            idip == 0 or  # Dip at beginning
            idip == len(nums)-2 or  # Dip at end
            nums[idip] <= nums[idip+2] or nums[idip-1] <= nums[idip+1]): # Dip in middle
            return True
        return False

print(Solution().checkPossibility([4,2,3])) # True
print(Solution().checkPossibility([4,2,1])) # False
print(Solution().checkPossibility([3,4,2,3])) # False