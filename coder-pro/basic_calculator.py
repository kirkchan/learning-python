""" 
    Not a leetcode standard question
"""
class Solution(object):
    def _eval(self, expression, i):
        res=0
        op='+'

        while i < len(expression):
            c = expression[i]

            # Operator
            if c in ('+', '-'):
                op = c
            else:
                value = 0
                if c.isdigit():
                    # value=int(c)

                    j=i+1
                    while j<len(expression):
                        if expression[j].isdigit():
                            j+=1
                            continue
                        else:
                            break
                    # print(expression[i:j])
                    value=int(expression[i:j])
                    if j<len(expression) and expression[j].isdigit():
                        i=j
                    else:
                        i=j-1
                elif c == '(':
                    (value, i) = self._eval(expression, i+1)
                
                if op == '+':
                    res += value
                else:
                    res -= value

            i+=1

        return (res, i)


    def calculate(self, s):
        return self._eval(s, 0)[0]
        
        

print(Solution().calculate("1 + 1")) # 2
print(Solution().calculate(" 2-1 + 2 ")) # 3
print(Solution().calculate("2147483647")) # 3
