class Solution():
    def twoSum(self, nums: [], target: int):
        seen = {}
        
        for i,n in enumerate(nums):
            print(target-n, seen)
            if target-n in seen:
                return [seen[target-n], i]
            
            seen[n] = i
        return []
                

    
print(Solution().twoSum([2,7,11,15], 9), '\n')
print(Solution().twoSum([2,7,11,15], 7), '\n')
print(Solution().twoSum([3,3], 6), '\n')

print(Solution().twoSum([4,7,1,-3,2], 5))
# True