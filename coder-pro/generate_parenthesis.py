class Solution(object):
    def generateParenthesis(self, n):
        ans = []

        def backtrack(entry, left, right):
            if len(entry) == 2*n:
                ans.append(entry)
                return
            
            if left < n:
                backtrack(entry+'(', left+1, right)
            if left > right
                backtrack(entry+')', left, right+1)

        backtrack('', 0, 0)
        return ans