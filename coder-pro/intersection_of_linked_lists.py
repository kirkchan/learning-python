# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        
        cursA = headA
        countA = 0
        cursB = headB
        countB = 0
        
        while cursA:
            countA += 1
            cursA = cursA.next
        while cursB:
            countB += 1
            cursB = cursB.next
            
        cursA = headA
        cursB = headB
        
        while countA > 0 or countB > 0:
            if id(cursA) == id(cursB):
                return cursA
            
            if countA > countB:
                countA -= 1
                cursA = cursA.next
            elif countA < countB:
                countB -= 1
                cursB = cursB.next
            else:
                countA -= 1
                cursA = cursA.next
                countB -= 1
                cursB = cursB.next
                
        return None
        
        