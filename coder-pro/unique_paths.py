class Solution(object):
    def uniquePaths(self, m, n):
        grid = []

        for i in range(n):
            grid.append([0]*m)

        for i in range(n):
            for j in range(m):
                if i == 0 or j == 0:
                    grid[i][j] = 1
                else:
                    grid[i][j] = grid[i-1][j] + grid[i][j-1]


        for i in range(n):
            print(grid[i])

        return grid[n-1][m-1]

print(Solution().uniquePaths(3,2)) # 3
print(Solution().uniquePaths(7,3)) # 28
