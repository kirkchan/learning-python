from collections import defaultdict

""" 
    Time    O(2n)
    Space   O(2n)

 """
class Solution(object):
    def pushDominoes(self, dominoes):
        N = len(dominoes)
        force = [0]*N

        f = 0
        for i in range(N):
            # print(dominoes[i])
            if dominoes[i] == 'R':
                f = N
                force[i] += f
            elif dominoes[i] == 'L':
                f = 0
            else:
                f = max(f-1, 0)
                force[i] += f
        
        for i in range(N-1, -1, -1):
            # print(i)
            if dominoes[i] == 'L':
                f = N
                force[i] -= f
            elif dominoes[i] == 'R':
                f = 0
            else:
                f = max(f-1, 0)
                force[i] -= f
            
        resp = []
        for i in range(N):
            if force[i] > 0:
                resp.append('R')
            elif force[i] < 0:
                resp.append('L')
            else:
                resp.append('.')

        return ''.join(resp)

print(Solution().pushDominoes(".L.R...LR..L.."))
                            # "LL.RR.LLRRLL.."
print(Solution().pushDominoes("...RL....R.L.L........RR......L....R.L.....R.L..RL....R....R......R.......................LR.R..L.R."))
                            # "...RL....R.LLL........RRRRRLLLL....R.L.....R.L..RL....RRRRRRRRRRRRRRRRRRRRRRRR.LLLLLLLLLLLLRRRRLL.RR"