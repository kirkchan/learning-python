class Solution(object):
    # def validPalindrome(self, s):
    #     i = 0
    #     j = len(s)-1
    #     passed = False

    #     left = True
    #     right = True
    #     while i<j:
    #         # print(s[i], s[j], passed)
    #         if s[i] != s[j]:
    #             if passed:
    #                 left = False
    #                 break

    #             # Check if i+1 is a match
    #             if s[i+1] == s[j]:
    #                 i+=1
    #                 passed = True
    #             # Two passes -> False
    #             else:
    #                 left = False
    #                 break
    #         i+=1
    #         j-=1

    #     i = 0
    #     j = len(s)-1
    #     passed = False
    #     while i<j:
    #         # print(s[i], s[j], passed)
    #         if s[i] != s[j]:
    #             if passed:
    #                 right = False
    #                 break

    #             # Check if j-1 is a match
    #             if s[i] == s[j-1]:
    #                 j-=1
    #                 passed = True
    #             # Two passes -> False
    #             else:
    #                 right = False
    #                 break
    #         i+=1
    #         j-=1
        
    #     return left or right

    def validPalindrome(self, s: str) -> bool:
        left, right = 0, len(s) - 1
        while left < right:
            if s[left] != s[right]:
                a = s[left:right]
                b = s[left + 1:right + 1]
                return a == a[::-1] or b == b[::-1]     
            else:
                left += 1
                right -= 1
        return True

print(Solution().validPalindrome('a')) # True
print(Solution().validPalindrome('ab')) # True
print(Solution().validPalindrome('aba')) # True
print(Solution().validPalindrome('acba')) # True
print(Solution().validPalindrome('abbda')) # True
print(Solution().validPalindrome('adbba')) # True
print(Solution().validPalindrome('abdbba')) # True
print(Solution().validPalindrome("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga")) # True
