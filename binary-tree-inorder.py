"""
    Trees-preorder is to learn the basic traversal.

    Given a pre-ordered list, print tree in-order

    RE-DO ENTIRELY
"""
class Node():
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    # Build tree recursively
    def buildTree(self, values):
        if len(values) == 0:
            return Node()
        
        root = Node()

        self._buildTreeHelper(root, values)
        self.tree = root

        return root

    """
        Node: current node in tree
        Values: values remaining
    """
    def _buildTreeHelper(self, node, values):

        if len(values) > 0:
            node.data = values.pop(0)
            # print("Created node: ", node.data)

        # Deadend node
        if node.data == None:
            return node
        
        # Children nodes
        if len(values) > 0:
            node.left = self._buildTreeHelper(Node(), values)
        if len(values) > 0:
            node.right = self._buildTreeHelper(Node(), values)

        return node





class Inorder():
    def __init__(self):
        pass

    def printTree(self, node):
        results = self._printTreeHelper(node, [])

        print(results)

    def _printTreeHelper(self, node, results):
        if node.left != None:
            self._printTreeHelper(node.left, results)

        if node.data != None:
            results.append(node.data)
            print("Hit node: ", node.data)

        if node.right != None:
            self._printTreeHelper(node.right, results)
            
        return results
    
    
    def printTreeIteratively(self, root):
        results = []
        stack = []

        stack.append(root)

        while len(stack) > 0:
            node = stack.pop()

            if node.left == None:
                results.append(node.data)

            if node.right == None:
                return
            

        

if __name__ == "__main__":
    # Basic traversal
    root = Node().buildTree([1,None,2,3])
    Inorder().printTree(root)
    # Inorder().printTreeIteratively(root)

    # # # Advance test case
    root = Node().buildTree([2,1,0,None,3,None,4])
    Inorder().printTree(root)
    # Inorder().printTree(root)