"""
    Graphs may contain cycles and thus require a mildly different solution
    to Depth-First Search with trees.

    A boolean array, visited, is needed to avoid cyclic references found within
    graphs.
"""
from collections import defaultdict

class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)

    def dfs(self, start):
        visited = [False] * (len(self.graph))
        stack = [start]
        resp = []

        print(self.graph.items())

        while stack:
            node = stack.pop()
            resp.append(node)
            visited[node] = True
            
            adjacent = self.graph[node]
            adjacent.reverse() # To preserve order in stack
            for adj in adjacent:

                if visited[adj] == False:
                    stack.append(adj)

        print(resp)


if __name__ == "__main__":
    g = Graph()

    g.addEdge(0, 1) 
    g.addEdge(0, 2) 
    g.addEdge(1, 2) 
    g.addEdge(2, 0) 
    g.addEdge(2, 3) 
    g.addEdge(3, 3)
    
    print ("Graph Depth-First Traversal from Vertex 2") 
    g.dfs(2)