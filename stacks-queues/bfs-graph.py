"""
    Graphs may contain cycles and thus require a mildly different solution
    to Breadth-First Search with trees.

    A boolean array, visited, is needed to avoid cyclic references found within
    graphs.
"""

from collections import defaultdict
from collections import deque

class Graph:

    def __init__(self):
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)

    def bfs(self, start):
        resp = []
        visited = [False] * (len(self.graph))
        queue = deque()
        queue.append(start) # Starting value

        # print(self.graph.items())

        while (queue):
            node = queue.popleft()
            adjacents = self.graph[node]
            # print(adjacents)

            resp.append(node)
            visited[node] = True

            for adj in adjacents:
                if visited[adj] == False:
                    queue.append(adj)
            
        print(resp)



if __name__ == "__main__":
    g = Graph()

    g.addEdge(0, 1) 
    g.addEdge(0, 2) 
    g.addEdge(1, 2) 
    g.addEdge(2, 0) 
    g.addEdge(2, 3) 
    g.addEdge(3, 3) 
    
    print ("Graph Breadth-First Traversal from Vertex 2") 
    g.bfs(2)