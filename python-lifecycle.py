print('''
    Python is a multi-functional programming language. It is first compiled 
    into bytecode and then intepreted by the VM, both commonly done in CPython.
    Nevertheless, it is a 'compiled interpreted' language.
''')

print("Before imports")

import math

print("After import; before functions")

def functionA():
    print("Hello, I am Function A")

print("Print statements can be evaluated at any time")

def functionB():
    print("I am the other function -- Function B!")

print("Before __name__ guard")

if __name__ == "__main__":
    functionA()
    functionB()

    print("This is special function equivalent to __main__.py for a package")

print("Final print statement")