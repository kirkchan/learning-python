import heapq

class KthLargest():
    def __init__(self, rank, arr):
        self.heap = arr
        self.rank = rank

    def add(self, node):
        self.heap.append(node)
        # print(self.rank, self.heap)
        resp = heapq.nlargest(self.rank, self.heap)

        print(resp[self.rank-1])
        return resp[self.rank-1]

if __name__ == "__main__":
    k = 3
    arr = [4,5,8,2]
    kthLargest = KthLargest(3, arr)
    kthLargest.add(3) # returns 4
    kthLargest.add(5) # returns 5
    kthLargest.add(10) # returns 5
    kthLargest.add(9) # returns 8
    kthLargest.add(4) # returns 8
