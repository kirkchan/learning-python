class ListNode:

    def __init__(self, data):
        self.data = data
        self.next = None

    def __str__(self):
        node = self
        output = ""
        while node:
            output += str(node.data) + " "
            node = node.next
        return output

def convertToLinkedList(l):
    if l == []:
        return None
    
    head = ListNode(l[0])
    crnt = head

    for i in range(1, len(l)):
        next = ListNode(l[i])
        crnt.next = next
        crnt = next
    
    return head

def reverse(l):

    head = ListNode(l.data)
    head.next = None

    crnt = l.next

    while crnt:
        temp = ListNode(crnt.data)
        temp.next = head
        head = temp

        crnt = crnt.next
    
    return head


if __name__ == '__main__':
    one = [1,3,5,7,9]
    two = [2,4,6,8,10]

    n1 = convertToLinkedList(one)
    n2 = convertToLinkedList(two)

    print("Created ", n1)
    print("Created ", n2)

    rev = reverse(n1)
    print("\n", rev)

    rev = reverse(n2)
    print("\n", rev)