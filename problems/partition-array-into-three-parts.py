class Solution(object):
    def canThreePartsEqualSum(self, A):
        sum = 0
        for i in A:
            sum += i
        
        target = sum/3

        sum = 0
        # print(target)
        partition = 0
        for i in range(len(A))  :
            sum += A[i]
            
            if sum == target:
                partition += 1
                sum = 0

        return partition == 3 


if __name__ == "__main__":
    print(Solution().canThreePartsEqualSum([3,3,6,5,-2,2,5,1,-9,4])) # True
    print(Solution().canThreePartsEqualSum([0,2,1,-6,6,7,9,-1,2,0,1])) # False
    print(Solution().canThreePartsEqualSum([0,2,1,-6,6,-7,9,1,2,0,1])) # True

