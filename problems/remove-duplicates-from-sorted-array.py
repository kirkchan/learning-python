# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def deleteDuplicates(self, ll):
        head = None
        tail = None

        last = None
        crnt = ll

        while crnt:
            # if last:
            #     print(last.val, crnt.val)
            # else:
            #     print('- {}'.format(crnt.val))

            # Skip duplicates
            if (crnt.next and crnt.val == crnt.next.val) or (last and last.val == crnt.val):
                pass

            # Different values
            else:
                # Fresh list
                if head == None:
                    head = crnt
                    tail = head
                # Update new list pointers
                else:
                    tail.next = crnt
                    print(tail.next.val)
                    tail = tail.next

            # Update pointers per iteration
            last = crnt
            crnt = crnt.next
        
        # For tailing duplicates
        if tail:
            tail.next = None
            
        return head

def printList(head):
    print('Currently: ', end=' ')
    crnt = head
    while crnt:
        print(crnt.val, end=' ')
        crnt = crnt.next
    print()

def buildList(arr):
    if not arr:
        print('EMPTY')
    head = ListNode(arr[0])
    crnt = head
    for n in arr[1:]:
        crnt.next = ListNode(n)
        crnt = crnt.next

    return head
    
if __name__ == "__main__":
    head = buildList([1,2,3,3,4,4,5])
    head = Solution().deleteDuplicates(head)
    printList(head)

    # head = buildList([1,2,2])
    # head = Solution().deleteDuplicates(head)
    # printList(head)

    # head = buildList([1,1])
    # head = Solution().deleteDuplicates(head)
    # printList(head)