import heapq

class Solution:
    def findDuplicates(self, nums):
        values={}
        for n in nums:
            if n in values:
                values[n] = values[n] + 1
            else:
                values[n] = 1

        i=0
        for v in values.values():
            if v == 2:
                i+=1
        nums = heapq.nlargest(i, values, key=values.get)
        # print(nums)

        return nums

if __name__ == "__main__":
    nums = [4,3,2,7,8,2,3,1]

    Solution().findDuplicates(nums)