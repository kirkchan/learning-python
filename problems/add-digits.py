"""
    Given non-negative number, repeatedly add the digits until it has one-digit

    Challenge: Do it without loops/recursion
"""
class Solution:
    def addDigits(self, num: int) -> int:
        sum = 0
        while num >= 0:
            digit = num%10
            num = int(num/10)
            sum += digit

            # print("Digit [{}]; Sum [{}]; Num [{}]".format(digit,sum,num))
            if num == 0:
                if sum < 10:
                    return sum
                else:
                    num = sum
                    sum = 0

if __name__ == "__main__":
    print(Solution().addDigits(38))