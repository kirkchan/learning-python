# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1: ListNode, l2: ListNode):
        res = ListNode(0)
        crnt = res

        sum = 0
        carryover = 0

        while l1 or l2:
            sum = carryover

            if l1:
                sum += l1.val
                l1 = l1.next
            if l2:
                sum += l2.val
                l2 = l2.next

            carryover = sum // 10
            remainder = sum % 10

            crnt.next = ListNode(remainder)
            crnt = crnt.next

        if carryover:
            crnt.next = ListNode(carryover)
            crnt = crnt.next
            
        return res.next
        
        
if __name__ == "__main__":
    pass

"""
    Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
    Output: 7 -> 0 -> 8
    Explanation: 342 + 465 = 807.

    Initiate 
        Head node of sum
        Carryover
        Current pointer
    Iterate between the lists
    If carryover, reset and add one to current sum
    Calculate sum and carryover
"""