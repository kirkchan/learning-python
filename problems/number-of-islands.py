"""
    Given a grid of land and water, calculate the number of islands in the 
    graph.

    Faster solution -- visited may be dropped; leverage grid to replace 
    (1 -> 0) values in place.

    Space   O(n*m)
    Runtime O(n*m)
"""
class Solution:
    def numIslands(self, grid) -> int:
        visited = []
        length = len(grid)
        width = len(grid[0])

        islands = 0
        
        # Initiate visited
        for i in range(length):
            visited.append([False]*width)
        # print(visited)

        i,j=0,0
        for i in range(length):
            for j in range(width):
                if not visited[i][j]:
                    # Landstruck; visit all of this land's units
                    if grid[i][j] == 1:
                        # print("\nHit land: [{} {}]".format(i,j))
                        islands += 1
                        self._dfsLand(i, j, length, width, grid, visited)

                    # # Waterstruck; mark and move on; no need to mark
                    
        return islands

    # Visits all land from the first point(i,j)
    def _dfsLand(self, i, j, length, width, grid, visited):
        # In-bounds
        if i < length and i >= 0 and j < width and j >= 0:
            # Visited previously; skip
            if visited[i][j]:
                return

            # New grid space
            coordinates = [[1,0], [-1,0], [0,1], [0,-1]]

            # Waterstruck; skip
            if grid[i][j] == 0:
                return

            # Mark new land
            visited[i][j] = True
            
            # Faster to call explicitly than to construct an array set of directional values
            self._dfsLand(i+1, j, length, width, grid, visited)
            self._dfsLand(i-1, j, length, width, grid, visited)
            self._dfsLand(i, j+1, length, width, grid, visited)
            self._dfsLand(i, j-1, length, width, grid, visited)

if __name__ == "__main__":
    solution = Solution()

    grid = [[1,1,1,1,0], [1,1,0,1,0], [1,1,0,0,0], [0,0,0,0,0,]]
    islands = solution.numIslands(grid)
    print(islands)

    grid = [[1,1,0,0,0], [1,1,0,0,0], [0,0,1,0,0], [0,0,0,1,1]]
    islands = solution.numIslands(grid)
    print(islands)