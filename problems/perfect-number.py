"""
    Given a positive integer, determine if the number is "perfect".

    Perfect: an integer equal to the sum all of its positive divisors, minus 
    the number itself

    Space   O(1)
    Runtime O(n)
"""
import math

class Solution:
    def checkPerfectNumber(self, num: int) -> bool:
        if num < 1:
            return False

        sqrt = int(math.ceil(num**0.5))
        sum = 0

        for n in range(1, sqrt):
            if num%n == 0:
                # print(n, num/n)
                sum = sum + n + num/n

        # Exclude self
        return (sum-num) == num


if __name__ == "__main__":
    solution = Solution()

    res = solution.checkPerfectNumber(28)
    print(res)

    res = solution.checkPerfectNumber(20996011)
    print(res)