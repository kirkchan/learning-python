"""
    Given an n x n matrix, rotate the matrix by 90d clockwise in-place
"""
class Solution:
    """
        How to calculate the 90d clockwise position...
            0   1   2
        0   1   2   3
        1   4   5   6
        2   7   8   9

        Flip x,y values
            0   1   2
        0   1   4   7
        1   2   5   8
        2   3   6   9

        Rev row
            0   1   2
        0   7   4   1
        1   8   5   2
        2   9   6   3
    """
    def rotate(self, matrix: list) -> None:
        length = len(matrix)
        width = len(matrix[0])

        for r in range(width):
            for c in range(r+1, length):
                temp = matrix[r][c]
                matrix[r][c] = matrix[c][r]
                matrix[c][r] = temp

            matrix[r].reverse()

if __name__ == "__main__":
    matrix = [
        [1,2,3],
        [4,5,6],
        [7,8,9]
    ]

    Solution().rotate(matrix)
    print(matrix)