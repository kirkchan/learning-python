class Solution(object):
    def generateParenthesis(self, n):
        ans = []
        
        def backtracing(entry, left, right):
            # print(len(entry), entry)
            if len(entry) == 2*n:
                # print('Exit condition')
                ans.append(entry)
                return
            
            if left < n:
                backtracing(entry+'(', left+1, right)
            if left > right:
                backtracing(entry+')', left, right+1)
                
        backtracing('', 0,0)
        
        return ans


if __name__ == "__main__":
    print(Solution().generateParenthesis(3))