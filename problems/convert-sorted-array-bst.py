# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

""" 
    if i == j
        return None
    Half of (i,j)
        Odd
        Even -> math.ceil

    math.ceil((i,half)/2) -> left
    math.ceil((half,j)/2) -> right
"""
import math
class Solution(object):
    def sortedArrayToBST(self, nums):
        if len(nums) == 0:
            return None
        if len(nums) == 1:
            return TreeNode(nums[0])

        mid = int(len(nums)/2)
        node = TreeNode(nums[mid])
        print(mid, nums)

        node.left = self.sortedArrayToBST(nums[0:mid])
        node.right = self.sortedArrayToBST(nums[mid+1:])

        return node

def dfs(root:TreeNode):
    if not root:
        # print(type(root))
        return
        
    print(root.val)
    dfs(root.left)
    dfs(root.right)

if __name__ == "__main__":
    arr = [-10,-3,0,5,9]
    root = Solution().sortedArrayToBST(arr)

    print("\nNext")
    dfs(root)    