class Solution(object):
    def longestPalindrome(self, s):
        if len(s) <= 1:
            return s

        longest = ''

        #Odd
        for i in range(len(s)):
            temp = self._findPalindrome(s, i, i)
            if len(temp) > len(longest):
                longest = temp

        # Even
        for i in range(1, len(s)):
            temp = self._findPalindrome(s, i-1, i)
            if len(temp) > len(longest):
                longest = temp

        return longest

    def _findPalindrome(self, string, a, b):
        i=a
        j=b
        res=''

        while i >= 0 and j < len(string):
            if string[i] != string[j]:
                return res

            res = string[i:j+1]
            i-=1
            j+=1
        
        return res

if __name__ == "__main__":
    print("Answer: [{}]".format(Solution().longestPalindrome("babad")))
    print("Answer: [{}]".format(Solution().longestPalindrome("cbbd")))
    print("Answer: [{}]".format(Solution().longestPalindrome("a")))
    print("Answer: [{}]".format(Solution().longestPalindrome("bb")))
    print("Answer: [{}]".format(Solution().longestPalindrome("ccc")))
    print("Answer: [{}]".format(Solution().longestPalindrome("dddd")))

"""
    Example 1
        Input: "babad"
        Output: "bab"
        Note: "aba" is also a valid answer.

    Example 2
        Input: "cbbd"
        Output: "bb" 
"""