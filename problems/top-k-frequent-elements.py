"""
    Given an array and k elements, return the k-most frequent elements

    Space   O(n)
    Time    O(n)
"""
import heapq

class Solution:
    def topKFrequent(self, nums, k):
        counts = {}

        for n in nums:
            if n in counts:
                counts[n] = counts[n] + 1
            else:
                counts[n] = 1
                
        return heapq.nlargest(k, counts, key=counts.get)

if __name__ == "__main__":
    print(Solution().topKFrequent([1,1,1,2,2,3], k=2))