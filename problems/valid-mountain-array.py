class Solution(object):
    def validMountainArray(self, A):
        if len(A) < 3:
            return False

        poi = 0
        direction = 1
        if A[1]-A[0] < 0:
            return False
        for i in range(1,len(A)):
            slope = A[i]-A[i-1]
            if slope == 0:
                return False
            if slope > 0 and direction == -1:
                poi += 1
                direction = 1
            if slope < 0 and direction == 1:
                poi += 1
                direction = -1

        return poi == 1 and direction == -1


if __name__ == "__main__":
    print(Solution().validMountainArray([2,1]))     # False
    print(Solution().validMountainArray([3,5,5]))   # False
    print(Solution().validMountainArray([0,3,2,1])) # True
    print(Solution().validMountainArray([2,0,2]))   # False
    print(Solution().validMountainArray([0,1,2,3,4,5,6,7,8,9]))   # False
    print(Solution().validMountainArray([2,1,2,3,5,7,9,10,12,14,15,16,18,14,13]))   # False
    print(Solution().validMountainArray([9,8,7,6,5,4,3,2,1,0]))   # False
