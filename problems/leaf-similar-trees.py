# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def leafSimilar(self, root1, root2):
        stack1 = self._findLeaves(root1)
        stack2 = self._findLeaves(root2)

        # print(stack1, stack2)
        
        while stack1 and stack2:
            node1 = stack1.pop()
            node2 = stack2.pop()            
            
            if node1.val != node2.val:
                return False
        
        # Both must be empty
        if len(stack1) != len(stack2):
            return False

        return True

    def _findLeaves(self, root):
        stack = [root]
        leaves = []

        while stack:
            node = stack.pop()
            
            if node != None:
                # print("Inspecting ", node.val)
                stack.append(node.right)
                stack.append(node.left)

                if node.left == None and node.right == None:
                    leaves.append(node)

        return leaves

if __name__ == "__main__":
    pass