class Solution:
    """
        Build a schedule first: min, max
        Mark the intervals of each time slot
        Find max overlaps
    """
    """
        Brute-force...
        Time    O(n**2)
        Space   O(n)
        Scales very poorly as schedules get longer
    """
    def minMeetingRoomsBrute(self, intervals) -> int:
        # Build schedule
        latest = 0
        for i in intervals:
            latest = max(latest, i[1])

        schedule = [0]*latest
        
        for i in intervals:
            for j in range(i[0], i[1]):
                schedule[j] += 1

        # Get the most conflicts
        conflicts = 0
        for i in schedule:
            conflicts = max(conflicts,i)

        return conflicts

    def minMeetingRooms(self, intervals):
        # Build intervals chronologically
        start=[]
        end=[]
        for i in intervals:
            start.append(i[0])
            end.append(i[1])

        start.sort()
        end.sort()

        s=0
        e=0
        open = 0
        rooms = 0
        while s < len(start):
            if start[s] < end[e]:
                if open:
                    open-=1
                else:
                    rooms+=1
                s+=1
            else:
                open+=1
                e+=1

        return rooms


if __name__ == "__main__":
    p1 = [[0,30],[5,10],[15,20]]
    p2 = [[7,10],[2,4]]
    p3 = [[10,20],[0,100],[4,7],[50,80],[90,10000000],[75,80],[79,90]]

    # a1 = Solution().minMeetingRooms(p1)
    # print('Correct' if a1 == 2 else 'Incorrect')

    # a2 = Solution().minMeetingRooms(p2)
    # print('Correct' if a2 == 1 else 'Incorrect')

    # a1 = Solution().minMeetingRoomsBrute(p1)
    # print('Correct' if a1 == 2 else 'Incorrect')

    # a2 = Solution().minMeetingRoomsBrute(p2)
    # print('Correct' if a2 == 1 else 'Incorrect')

    a3 = Solution().minMeetingRooms(p3)
    # a3 = Solution().minMeetingRoomsBrute(p3)
    print('Correct' if a3 == 4 else 'Incorrect')