class Solution(object):
    def threeSum(self, nums):
        nums.sort()
        # print('\nTest case ',nums)
        length = len(nums)
        ans = []

        for i in range(length-2):
            # Skip duplicate numbers
            if i > 0 and nums[i] == nums[i-1]:
                continue

            # print('i =',i)
            j=i+1
            k=length-1

            while j < k:
                # print(nums[i], nums[j], nums[k],'\n')
                if nums[i]+nums[j]+nums[k] == 0:
                    # print('Answer at indices:', i,j,k, '\n')
                    ans.append([nums[i], nums[j], nums[k]])
                    
                    # Check for other solutions with the range
                    while j < k and nums[j] == nums[j+1]:
                        j+=1
                    while j < k and nums[k] == nums[k-1]:    
                        k-=1

                    j+=1
                    k-=1
                elif nums[i]+nums[j]+nums[k] < 0:
                    j+=1
                elif nums[i]+nums[j]+nums[k] > 0:
                    k-=1

        return ans


if __name__ == "__main__":
    print(Solution().threeSum([-1, 0, 1, 2, -1, -4]))
    print(Solution().threeSum([0, 0, 0, 0]))
    print(Solution().threeSum([-2,0,1,1,2]))
    