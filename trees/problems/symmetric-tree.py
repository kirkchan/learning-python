"""
    Given an array representation of a tree, determine if the tree is 
    symmetrical in values, down the center

    Solution: The input is level-ordered. Each level contains 2^n number of 
    nodes, where n is the level. With a little math, the indices of each level
    can be checked for symmetry.
"""

def _isSymmetrical(tree):
    length = len(tree)
    if length < 2:
        return True
    
    ptr = 1 # First index of level
    i = 1 # Level 2 of tree
    while True:
        min = ptr
        max = ptr + 2**i
        ptr = max

        # print("Min: ", min, "; Max: ", max, "; Length: ", length)
        if max > length:
            return False

        x = int(2**i/2)
        # print("Iterations to run: ", x)
        for n in range(x):
            # print("Index: ", n)
            i = min + n
            j = max - n - 1
            # print("tree[{}] != tree[{}]?".format(i, j))
            if tree[i] != tree[j]:
                return False

        if max == length:
            break

        i += 1

    return True

def checkSymmetry(tree):
    ans = _isSymmetrical(tree)  
    print(ans, ": ", tree)

if __name__ == "__main__":
    tree = [1,2,2,3,4,4,3]
    checkSymmetry(tree)

    tree = [1,2,2,None,3,None,3]
    checkSymmetry(tree)