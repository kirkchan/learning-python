"""
    Given an array representation of a tree, calculate the maxium height of the
    tree

    The efficient solution runs at O(n) where a height array is created. And 
    from this information, the max height can be calculated.

    NOTE The array is level-ordered.
"""

"""
    Using array index from 0,
        Parent = i
        Left = 2*i + 1
        Right = 2*i + 2

    Knowing these properties, I can skip the construction of the tree and 
    calculate the longest link of nodes.
"""
def findMaxDepth(nodeList):
    longest = 0
    
    if len(nodeList) < 2:
        return longest

    nodeHeights = _calculateHeights(nodeList)

    for n in nodeHeights:
        if n != None:
            longest = max(n, longest)

    return longest

def _calculateHeights(nodeList):
    length = len(nodeList)
    heights = [1 for i in range(length)]

    for i in range(length):

        left = 2*i + 1  # Left index
        right = 2*i + 2 # Right index

        if left < length:
            if nodeList[left] != None:
                heights[left] = heights[i]+1
            else:
                heights[left] = None

        if right < length:
            if nodeList[right] != None:
                heights[right] = heights[i]+1
            else:
                heights[right] = None
    
    return heights
    
if __name__ == "__main__":
    nodeList = [1,2,3,None,4,5,6,None,None,7]
    ans = findMaxDepth(nodeList)

    print(ans)