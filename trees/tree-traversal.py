"""
    Solutions to show how to traverse trees:
        
        - BFS
        - DFS: preorder
"""

class Node(object):
    def __init__(self, data=None, children=None):
        self.data = data
        self.children = children


    # BREADTH FIRST SEARCH
def bfsRecursive(node):
    queue = [node]
    _bfsRecursiveHelper(queue)

def _bfsRecursiveHelper(queue):
    # Need a stopper
    if len(queue) == 0:
        return
    
    node = queue.pop(0)
    print(node.data, end = ' ')

    # Queue children
    if node.children != None:
        for child in node.children:
            queue.append(child)
            # print('Queuing ', child.data)
        
        # print('Queue length: ', len(queue))
    
    # Repeat
    _bfsRecursiveHelper(queue)

# Very similar after done recursively
def bfsIterative(root):
    queue = [root]

    while len(queue) > 0:
        node = queue.pop(0)
        print(node.data, end = ' ')

        if node.children != None:
            for child in node.children:
                queue.append(child)


# DEPTH FIRST SEARCH

def dfsRecursive(node):
    _dfsRecursiveHelper(node)

def _dfsRecursiveHelper(node):
    # Print current node data first
    print(node.data, end = ' ')

    # Traverse through children nodes
    if node.children != None:
        for child in node.children:
            _dfsRecursiveHelper(child)

# Iterative DFS is slightly different
def dfsIterative(root):
    # DFS priorities as it traverses down, then right. Hence, stacks are used.
    stack = [root]

    while len(stack) > 0:
        # Always print node data first
        node = stack.pop()
        print(node.data, end = ' ')

        if node.children != None:
            # Order must be reversed because of stack order
            ch = []
            for child in node.children:
                ch.append(child)
            ch.reverse()

            # Put reversed children onto stack
            for child in ch:
                    stack.append(child)

            """
            Now repeat starting with top child on stack
            Again, prioritize traversal down, then right
            """

if __name__ == "__main__":
    a = Node('a')
    b = Node('b')
    c = Node('c')
    d = Node('d')
    e = Node('e')
    f = Node('f')

    a.children = [b,c,d]
    b.children = [e,f]

    print("BFS Recursive")
    bfsRecursive(a)
    print("\n\n")

    print("BFS Iterative")
    bfsIterative(a)
    print("\n\n")

    print("DFS Recursive")
    dfsRecursive(a)
    print("\n\n")

    print("DFS Iterative")
    dfsIterative(a)
    print("\n\n")