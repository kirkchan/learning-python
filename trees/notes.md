# Overview

Trees are a keystone to problem solving. There are two common types when traversing a tree:

 * Breadth-first search (or level-order traversal)
 * Depth-first search
 
Within the DFS, there are three different approaches:
 
 * Inorder (left-node-right)
 * Preorder (node-left-right)
 * Postorder (left-right-node)
 
These three names are relative to the position of node when reading the tree from left-to-right. The most common DFS is preorder.
 
## Tree Properties
 
A node of a tree have at least two properties:
 
 * Value: the node data
 * Children: references to other nodes can be stored as an array or explicit values
 
### Binary Trees

Here is a recap of verbage with binary trees:

| Property | Description |
| :------: | ----------- |
| Binary tree | A tree type with up to two children |
| Binary search tree | An ordered binary tree where the left subtrees have smaller values than their parent and the right subtrees have greater values than their parent |
| Full binary tree | A binary tree where every node has either 0 or 2 children |
| Complete binary tree | A binary tree where the last level is filled, except the last right-most nodes |
| Perfect binary tree | A binary tree that is full and the leaves are at the same level |
| Leaf node | A node with no children |

### Binary Tree Computations

Here are some computations of a binary tree:

| Computation | Forumula |
| :---------: | -------- |
| Array Representation | *Offset 0* <br><br>Parent = i <br>Left = 2*i+1 <br>Right = 2*i+2 |
