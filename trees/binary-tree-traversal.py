"""
    Solutions to show how to perform iterative and recursive DFS traversal on 
    binary trees:

        - Preorder
        - Inorder
        - Postorder
"""

class Node():
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right



# PREORDER
def preorder(node):
    print('Preorder Recursive: ', end = '')
    _preorderHelper(node)
    print()

def _preorderHelper(node):
    print(node.data, end = ' ')

    if node.left != None:
        _preorderHelper(node.left)
    if node.right != None:
        _preorderHelper(node.right)

def preorderIteratively(node): 
    stack = []
    stack.append(node)
    print('Preorder Iterative: ', end = '')

    while len(stack) > 0:
        crnt = stack.pop()
        print(crnt.data, end = ' ')

        # MUST stack in reverse order
        if(crnt.right != None):
            stack.append(crnt.right)
        if(crnt.left != None):
            stack.append(crnt.left)

    print()



# INORDER
def inorder(node):
    print('Inorder Recursive: ', end = '')
    _inorderHelper(node)
    print()

def _inorderHelper(node):
    if node.left != None:
        _inorderHelper(node.left)

    print(node.data, end = ' ')

    if node.right != None:
        _inorderHelper(node.right)

def inorderIteratively(node):
    stack=[]
    crnt = node
    print('Inorder Iterative: ', end = '')

    while True:
        if crnt != None:
            stack.append(crnt)
            crnt = crnt.left

        elif len(stack) > 0:
            crnt = stack.pop()
            print(crnt.data, end = ' ')
            crnt = crnt.right

        else:
            break
        
    print()



# POSTORDER
def postorder(node):
    print('Postorder Recursive: ', end = '')
    _postorderHelper(node)
    print()

def _postorderHelper(node):
    if node.left != None:
        _postorderHelper(node.left)
    if node.right != None:
        _postorderHelper(node.right)

    print(node.data, end = ' ')

"""
    NOTE Iterative postorder is the trickiest of the three iterative solutions.
    The crnt value has to prioritize left, right, and then the parent Node.
    The key is to conditionally run the nested loop when to traverse left and
    always pop from stack after None is hit.
"""
def postorderIteratively(node):
    stack = []
    crnt = node
    print('Postorder Iterative: ', end = '')

    i = 0
    while True and i<30:
        i+=1

        # Traverse to the left most node until None, queuing right > crnt
        while crnt != None:
            if crnt.right != None:
                stack.append(crnt.right)
            stack.append(crnt)
            crnt = crnt.left

        # Crnt hit a 'None' node; grab the last node
        crnt = stack.pop()

        # If there is a right node matching the stack, then stack crnt and
        # move to right
        if crnt.right != None and len(stack) > 0 and stack[-1] == crnt.right:
            stack.pop()
            stack.append(crnt)
            crnt = crnt.right

        # No right node, print crnt and traverse to higher node
        else:
            print(crnt.data, end = ' ')
            crnt = None # This is the trick with the nested while loop

        if len(stack) < 1:
            break

    print()



if __name__ == "__main__":
    a = Node('a')
    b = Node('b')
    c = Node('c')
    d = Node('d')
    e = Node('e')
    f = Node('f')

    g = Node('g')

    a.left = b
    a.right = c
    
    b.left = d
    b.right = e

    e.left = g

    c.left = f

    preorder(a)
    preorderIteratively(a)

    inorder(a)
    inorderIteratively(a)

    postorder(a)
    postorderIteratively(a)